require("dotenv").config();
const express = require("express");
const app = express();
const server = app.listen(process.env.PORT, () => {
    console.log("server up and running on PORT :", process.env.PORT);
});
app.use("/", express.static("./build"));
