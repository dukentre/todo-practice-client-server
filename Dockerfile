FROM node:14
WORKDIR /nodeProjects/todo-practice-client-server
COPY . /nodeProjects/todo-practice-client-server
RUN npm i typescript -g
RUN yarn
RUN ls
RUN tsc --build ./tsconfig.json
ENV PORT 2555
EXPOSE 2555
CMD ["node","app.js"]
